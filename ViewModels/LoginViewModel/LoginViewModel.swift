//
//  LoginViewModel.swift
//  TestingServer
//
//  Created by Luan Nguyen on 11/19/15.
//  Copyright © 2015 Luan Nguyen. All rights reserved.
//

import ReactiveCocoa
import Models

public class LoginViewModel: LoginViewModeling {
  
  public var account: AnyProperty<Account?> {
    return AnyProperty(_account)
  }
  
  public var isLogon = MutableProperty<Bool>(false)
  private var _account = MutableProperty<Account?>(nil)
  
  private let accountModel: AccountModeling
  public init(accountModel: AccountModeling) {
    self.accountModel = accountModel
  }
  
  // Check if user signed in to the app before
  public func checklogin() {
    self._account.value = accountModel.checkLogin()
  }
  
  // Send a request to server to check login and get user informations
  public func login(email: String, passwd: String) {
    accountModel.login(email, passwd: passwd)
      .start({ event in
        switch event {
        case .Next(let acc):
          self._account.value = acc
          break
        case .Failed(let error):
          print(error)
          break
        default:
          break
        }
      })
  }
}
