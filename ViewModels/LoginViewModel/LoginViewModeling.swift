//
//  LoginViewModeling.swift
//  TestingServer
//
//  Created by Luan Nguyen on 11/19/15.
//  Copyright © 2015 Luan Nguyen. All rights reserved.
//

import ReactiveCocoa
import Models

public protocol LoginViewModeling {
  var account: AnyProperty<Account?>{get}
  
  // Check logged-in user
  func checklogin()
  
  // Login with email/password
  func login(email: String, passwd: String)
}

public protocol LoginViewModifiable: LoginViewModeling {
  
}
