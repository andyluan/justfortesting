//
//  OpeningTime.swift
//  TestingServer
//
//  Created by Luan Nguyen on 11/19/15.
//  Copyright © 2015 Luan Nguyen. All rights reserved.
//

import ObjectMapper

public class OpeningTime: NSObject {
  
  var id: Int?
  var weekday: String?
  var start_time: String?
  var end_time: String?
  
  required convenience public init?(_ map: Map) {
    self.init()
    mapping(map)
  }
  
}

extension OpeningTime: Mappable {
  public func mapping(map: Map) {
    id <- map["id"]
    weekday <- map["weekday"]
    start_time <- map["start_time"]
    end_time <- map["end_time"]
  }
}
