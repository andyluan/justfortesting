//
//  ImageModel.swift
//  TestingServer
//
//  Created by Luan Nguyen on 11/19/15.
//  Copyright © 2015 Luan Nguyen. All rights reserved.
//

import Foundation
import ObjectMapper

public class Photo: NSObject {
  
  var small: String?
  var medium: String?
  var original: String?
  
  required convenience public init?(_ map: Map) {
    self.init()
    mapping(map)
  }
}

extension Photo: Mappable {
  public func mapping(map: Map) {
    small <- map["small"]
    medium <- map["medium"]
    original <- map["original"]
  }
}
