//
//  Address.swift
//  TestingServer
//
//  Created by Luan Nguyen on 11/19/15.
//  Copyright © 2015 Luan Nguyen. All rights reserved.
//

import ReactiveCocoa
import ObjectMapper

public class Address: NSObject {
  
  var id: Int?
  var street: String?
  var apt: String?
  var city: String?
  var state: String?
  var country: String?
  var zipcode: String?
  var postalcode: String?
  var latitude: NSNumber?
  var longtitude: NSNumber?
  
  required convenience public init?(_ map: Map) {
    self.init()
    
    mapping(map)
  }
}

extension Address: Mappable{
  public func mapping(map: Map) {
    id <- map["id"]
    street <- map["street"]
    apt <- map["apt"]
    city <- map["city"]
    state <- map["state"]
    country <- map["country"]
    zipcode <- map["zipcode"]
    postalcode <- map["postalcode"]
    longtitude <- map["longtitude"]
    latitude <- map["latitude"]
  }
}
