//
//  AccountModel.swift
//  TestingServer
//
//  Created by Luan Nguyen on 11/19/15.
//  Copyright © 2015 Luan Nguyen. All rights reserved.
//

import ReactiveCocoa
import ObjectMapper
import Commons

public final class AccountModel: AccountModeling {

  public static let singleton = AccountModel()
  private init(){}
  
  public var account: Account?
  
  private var network: Networking?
  public convenience init(network: Networking) {
    self.init()
    self.network = network
  }
  
  var url: String {
    get {
      return "" + "authentication/"
    }
  }
  
  public func login(email:String, passwd: String) -> SignalProducer<Account, NetworkError> {
    let params = ["email": email, "password": passwd]
    return SignalProducer { observer, disposable in
      self.network!.POST(url: self.url, parameters: params)
        .start({ event in
          switch (event) {
          case .Next(let json):
            if let account = Mapper<Account>().map(json.description){
              self.saveAccount(account)
              observer.sendNext(account)
            }
          case .Failed(let error):
            observer.sendFailed(error)
          default:
            observer.sendCompleted()
          }
        })
    }
  }
  
  public func checkLogin() -> Account? {
    let userdefault = NSUserDefaults.standardUserDefaults()
    if let token = userdefault.valueForKey("token") {
      self.account = Account(id: userdefault.valueForKey("id") as! Int, token: token as! String)
    }
    return self.account
  }
  
  private func saveAccount(acc: Account) {
    let userdefault = NSUserDefaults.standardUserDefaults()
    userdefault.setValue(acc.id, forKey: "id")
    userdefault.setValue(acc.token, forKey: "token")
    self.account = acc
  }
  
  
}
