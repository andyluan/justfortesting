//
//  AccountModeling.swift
//  TestingServer
//
//  Created by Luan Nguyen on 11/19/15.
//  Copyright © 2015 Luan Nguyen. All rights reserved.
//

import ReactiveCocoa
import Commons

public protocol AccountModeling {
  
  func login(email: String, passwd: String) -> SignalProducer<Account, NetworkError>
  
  func checkLogin() -> Account?
  
}
