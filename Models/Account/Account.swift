//
//  Account.swift
//  TestingServer
//
//  Created by Luan Nguyen on 11/19/15.
//  Copyright © 2015 Luan Nguyen. All rights reserved.
//

import Foundation
import ObjectMapper

public class Account: NSObject {
  
  public var id: Int?
  var username: String?
  var email: String?
  public var token: String?
  var first_name: String?
  var last_name: String?
  
  var full_name : String? {
    get{
      guard let _ = first_name, _ = last_name else {
        return ""
      }
      return self.first_name! + " " + self.last_name!
    }
  }
  
  var avatar: Photo?
  
  convenience public init(id: Int, token: String) {
    self.init()
    self.id = id
    self.token = token
  }
  
  required convenience public init?(_ map: Map) {
    self.init()
    mapping(map)
  }
}

extension Account: Mappable {
  public func mapping(map: Map) {
    id <- map["id"]
    email <- map["email"]
    username <- map["username"]
    token <- map["access_token"]
    first_name <- map["first_name"]
    last_name <- map["last_name"]
    avatar <- map["image"]
  }
}
