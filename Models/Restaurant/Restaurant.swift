//
//  Restaurant.swift
//  TestingServer
//
//  Created by Luan Nguyen on 11/19/15.
//  Copyright © 2015 Luan Nguyen. All rights reserved.
//

import ObjectMapper

public class Restaurant: NSObject {
  
  var id: Int?
  var name: String?
  var address: Address?
  var tel: String?
  var is_available: Bool?
  var cover: [Photo]?
  var openingtimes: [OpeningTime]?
  
  required convenience public init?(_ map: Map) {
    self.init()
    mapping(map)
  }
}

extension Restaurant: Mappable {
  public func mapping(map: Map) {
    id <- map["id"]
    name <- map["name"]
    tel <- map["tel"]
    address <- map["address"]
    is_available <- map["is_available"]
    openingtimes <- map["opening_time"]
    cover <- map["image"]
  }
}
