//
//  RestaurantModel.swift
//  TestingServer
//
//  Created by Luan Nguyen on 11/19/15.
//  Copyright © 2015 Luan Nguyen. All rights reserved.
//

import ReactiveCocoa
import ObjectMapper
import Commons

public final class RestaurantModel: RestaurantModeling {
  
  private let network: Networking
  public init(network: Networking){
    self.network = network
  }
}
