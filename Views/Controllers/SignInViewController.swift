//
//  SignInViewController.swift
//  TestingServer
//
//  Created by Luan Nguyen on 11/19/15.
//  Copyright © 2015 Luan Nguyen. All rights reserved.
//

import UIKit
import ReactiveCocoa
import ViewModels
import Commons

public class SignInViewController: UIViewController {
  
  @IBOutlet weak var emailTextField: UITextField!
  @IBOutlet weak var passwordTextField: UITextField!
  @IBOutlet weak var loginButton: UIButton!
  
  public var loginViewModel: LoginViewModeling?
  
  override public func viewDidLoad() {
    super.viewDidLoad()
    
    // Binding viewmodels and ui controller
    bindingViewModel()
    
    // Check if logged-in user
    if let _ = loginViewModel {
      loginViewModel!.checklogin()
    }
  }
  
  func bindingViewModel() {
    
    guard let loginViewModel = loginViewModel else {
      return
    }

    loginViewModel.account.producer
      .on(next: { acc in
        if let _ = acc {
          self.performSegueWithIdentifier("showRestaurantViewController", sender: self)
        }
      })
      .start()
    
    loginButton.rac_enabled <~ combineLatest(emailTextField.textSignal().producer, passwordTextField.textSignal().producer).map { email, pass in
      return !email.isEmpty && !pass.isEmpty
    }
    
  }
  
  @IBAction func didTapLoginButton(sender: AnyObject) {
    if let _ = loginViewModel {
      loginViewModel?.login(emailTextField.text!, passwd: passwordTextField.text!)
    }
  }
}
