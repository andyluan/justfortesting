//
//  ResponseToJSON.swift
//  MVVM-IOS
//
//  Created by Luan Nguyen on 10/8/15.
//  Copyright © 2015 Asnet. All rights reserved.
//

import Alamofire

extension Request {
	
	func responseToJSON(completionHandler: (NSURLRequest, NSHTTPURLResponse?, JSON, ErrorType?) -> Void) -> Self {
		
		return response { res in
			dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), {
				
				var responseJSON : JSON
				if let data = res.2 {
					responseJSON = JSON(data: data)
				} else {
					responseJSON = JSON.null
				}
				dispatch_async(dispatch_get_main_queue(), {
					completionHandler(res.0!, res.1, responseJSON, res.3)
				})
			})
		}
	}
	
	func responseToData(completionHandler: (NSURLRequest, NSHTTPURLResponse?, Result<NSData, ErrorType>) -> Void) -> Self {
		return response { res in
			dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), { () -> Void in
					let result : Result<NSData, ErrorType>
					if let error = res.3 {
						result = Result.Failure(error)
					} else {
						result = Result.Success(res.2!)
					}
				dispatch_async(dispatch_get_main_queue(), { () -> Void in
					completionHandler(res.0!, res.1, result)
				})
			})
		}
	}

}

extension NSObject {
	func checkErrorResponse(response: NSHTTPURLResponse?) -> Bool {
		return response?.statusCode >= 200 && response?.statusCode < 300
	}
}
