//
//  Util.swift
//

import UIKit
import ReactiveCocoa
import Swinject

struct AssociationKey {
  static var hidden: UInt8 = 1
  static var alpha: UInt8 = 2
  static var text: UInt8 = 3
  static var image: UInt8 = 4
	static var enabled: UInt8 = 0
}

// lazily creates a gettable associated property via the given factory
func lazyAssociatedProperty<T: AnyObject>(host: AnyObject, key: UnsafePointer<Void>, factory: ()->T) -> T {
  var associatedProperty = objc_getAssociatedObject(host, key) as? T
  
  if associatedProperty == nil {
    associatedProperty = factory()
    objc_setAssociatedObject(host, key, associatedProperty, objc_AssociationPolicy.OBJC_ASSOCIATION_RETAIN)
  }
  return associatedProperty!
}

func lazyMutableProperty<T>(host: AnyObject, key: UnsafePointer<Void>, setter: T -> (), getter: () -> T) -> MutableProperty<T> {
  return lazyAssociatedProperty(host, key: key) {
    let property = MutableProperty<T>(getter())
    property.producer
		.startWithNext({ value in
			setter(value)
		})
    return property
  }
}

extension UIView {
  public var rac_alpha: MutableProperty<CGFloat> {
    return lazyMutableProperty(self, key: &AssociationKey.alpha, setter: { self.alpha = $0 }, getter: { self.alpha  })
  }
  
  public var rac_hidden: MutableProperty<Bool> {
    return lazyMutableProperty(self, key: &AssociationKey.hidden, setter: { self.hidden = $0 }, getter: { self.hidden  })
  }
}

extension UIImageView {
  public var rac_image: MutableProperty<UIImage?> {
    return lazyMutableProperty(self, key: &AssociationKey.image, setter: { self.image = $0 }, getter: { self.image })
  }
}

extension UILabel {
  public var rac_text: MutableProperty<String> {
    return lazyMutableProperty(self, key: &AssociationKey.text, setter: { self.text = $0 }, getter: { self.text ?? "" })
  }
}

extension UITextField {
  public var rac_text: MutableProperty<String> {
    return lazyAssociatedProperty(self, key: &AssociationKey.text) {
      
      self.addTarget(self, action: "changed", forControlEvents: .EditingChanged)
      
      let property = MutableProperty<String>(self.text ?? "")
      property.producer
				.startWithNext({ value in
          self.text = value
        })
      return property
    }
  }
  
  func changed() {
    rac_text.value = self.text!
  }
	
	public func textSignal() -> SignalProducer<String, NoError> {
		return self.rac_textSignal().toSignalProducer()
					.map{ $0! as! String}
					.flatMapError {_ in SignalProducer(value: "")}
	}
}

extension UITextView {
	public var rac_text: MutableProperty<String> {
		return lazyAssociatedProperty(self, key: &AssociationKey.text) {
			
			let property = MutableProperty<String>(self.text ?? "")
			property.producer
				.startWithNext({ value in
					self.text = value
				})
			return property
		}
	}
	
	public func textSignal() -> SignalProducer<String, NoError> {
		return self.rac_textSignal().toSignalProducer()
			.map{ $0! as! String}
			.flatMapError {_ in SignalProducer(value: "")}
	}
}

extension UIButton {
	public var rac_enabled: MutableProperty<Bool> {
		return lazyMutableProperty(self, key: &AssociationKey.enabled, setter: { self.enabled = $0 }, getter: { self.enabled  })
	}
}
