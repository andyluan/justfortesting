//
//  UIKitExtensions.swift
//  MVVM-IOS
//
//  Created by Luan Nguyen on 10/8/15.
//  Copyright © 2015 Asnet. All rights reserved.
//

import Foundation
import Swinject

typealias MethodSwizzlingViewController = UIViewController
typealias UIKitViewController = UIViewController

extension UIKitViewController {
	public func displayErrorMessage(errorMessage: String) {
		let title = LocalizedString("ImageSearchTableViewController_ErrorAlertTitle", comment: "Error alert title.")
		let dismissButtonText = LocalizedString("ImageSearchTableViewController_DismissButtonTitle", comment: "Dismiss button title on an alert.")
		let message = errorMessage
		let alert = UIAlertController(title: title, message: message, preferredStyle: .Alert)
		alert.addAction(UIAlertAction(title: dismissButtonText, style: .Default) { _ in
			alert.dismissViewControllerAnimated(true, completion: nil)
			})
		self.presentViewController(alert, animated: true, completion: nil)
	}
}

extension MethodSwizzlingViewController {
	private struct AssociatedKeys {
		static var DescriptiveName = "nsh_DescriptiveName"
	}
	
	var descriptiveName: String? {
		get {
			return objc_getAssociatedObject(self, &AssociatedKeys.DescriptiveName) as? String
		}
		
		set {
			if let newValue = newValue {
				objc_setAssociatedObject(
					self,
					&AssociatedKeys.DescriptiveName,
					newValue as NSString?,
					.OBJC_ASSOCIATION_RETAIN_NONATOMIC
				)
			}
		}
	}
	
	public override class func initialize() {
		struct Static {
			static var token: dispatch_once_t = 0
		}
		
		// make sure this isn't a subclass
		if self !== UIViewController.self {
			return
		}
		
		dispatch_once(&Static.token) {
			let originalSelector = Selector("viewDidLoad")
			let swizzledSelector = Selector("swl_viewDidLoad")
			
			let originalMethod = class_getInstanceMethod(self, originalSelector)
			let swizzledMethod = class_getInstanceMethod(self, swizzledSelector)
			
			let didAddMethod = class_addMethod(self, originalSelector, method_getImplementation(swizzledMethod), method_getTypeEncoding(swizzledMethod))
			
			if didAddMethod {
				class_replaceMethod(self, swizzledSelector, method_getImplementation(originalMethod), method_getTypeEncoding(originalMethod))
			} else {
				method_exchangeImplementations(originalMethod, swizzledMethod);
			}
		}
	}
	
	// MARK: - Method Swizzling
	
	func swl_viewWillAppear(animated: Bool) {
		self.swl_viewWillAppear(animated)
		if let name = self.descriptiveName {
			print("viewWillAppear: \(name)")
		} else {
			print("viewWillAppear: \(self)")
		}
	}
	
	func swl_viewDidLoad() {
		self.swl_viewDidLoad()
		if let name = self.descriptiveName {
			print("viewDidLoad: \(name)")
		} else {
			print("viewDidLoad: \(self)")
		}
	}
}


