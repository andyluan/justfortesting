//
//  Converting.swift
//  TestingServer
//
//  Created by Luan Nguyen on 11/19/15.
//  Copyright © 2015 Luan Nguyen. All rights reserved.
//

import Foundation

extension String {
    func convertToDate() -> NSDate? {
        let dateFormatter = NSDateFormatter()
        dateFormatter.dateFormat = "hh:mm:ss"
        let date = dateFormatter.dateFromString(self)
        if (date != nil) {
            return date
        }
        return nil
    }
}
