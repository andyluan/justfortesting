//
//  enumations.swift
//  MVVM-IOS
//
//  Created by Luan Nguyen on 9/25/15.
//  Copyright © 2015 Asnet. All rights reserved.
//

import Foundation

enum EventType: Int {
	case HomeFeed = 1
	case Explore, Past, ByUser
}

enum Result<T, U> {
	case Success(T)
	case Failure(U)
}

enum EventError: ErrorType {
	case ObjectNotExist
	case ObjectRemoved
	case OutOfIndexBounds
	case Unknown
}