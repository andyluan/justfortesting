//
//  Networking.swift
//  MVVM-IOS
//
//  Created by Luan Nguyen on 9/24/15.
//  Copyright © 2015 Asnet. All rights reserved.
//

import ReactiveCocoa

public protocol Networking {
	
	// Method GET
	func GET(url url: String, parameters: [String : AnyObject]?) -> SignalProducer<JSON, NetworkError>
	
	// Method POST
	func POST(url url: String, parameters: [String : AnyObject]?) -> SignalProducer<JSON, NetworkError>
	
	// Method PUT
	func PUT(url url: String, parameters: [String : AnyObject]?) -> SignalProducer<JSON, NetworkError>
	
	// Method DELETE
	func DELETE(url url: String, parameters: [String : AnyObject]?) -> SignalProducer<JSON, NSError>
	
	// Method Multipart
	func Multipart(urlString: String, parameters: Dictionary<String, AnyObject>?, imageData: NSData) -> SignalProducer<JSON, NetworkError>
}