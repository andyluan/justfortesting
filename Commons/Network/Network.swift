//
//  Network.swift
//  TestingServer
//
//  Created by Luan Nguyen on 11/20/15.
//  Copyright © 2015 Luan Nguyen. All rights reserved.
//

import ReactiveCocoa
import Alamofire
import Foundation
//import Models

public class Network: NSObject, Networking {
  private var fakeError: ErrorType {
    return NSError(domain: NSURLErrorDomain, code: 400, userInfo: nil)
  }
  
  public override init() { }
  
  public func GET(url url: String, parameters: [String : AnyObject]?) -> SignalProducer<JSON, NetworkError> {
//    if let account = AccountModel.singleton.account {
//      Alamofire.Manager.sharedInstance.session.configuration.HTTPAdditionalHeaders = ["Api-Key": "\(account.token)"]
//    }
    return SignalProducer { observer, disposable in
      Alamofire.request(.GET, url, parameters: parameters)
        .responseToJSON({ (_, res, json, error) in
          if error != nil || !self.checkErrorResponse(res) {
            observer.sendFailed(NetworkError(error: error ?? self.fakeError))
          } else {
            observer.sendNext(json)
            observer.sendCompleted()
          }
        })
    }
  }
  
  public func POST(url url: String, parameters: [String : AnyObject]?) -> SignalProducer<JSON, NetworkError> {
//    if let account = AccountModel.singleton.account {
//      Alamofire.Manager.sharedInstance.session.configuration.HTTPAdditionalHeaders = ["Api-Key": "\(account.token)"]
//    }
    return SignalProducer { observer, disposable in
      Alamofire.request(.POST, url, parameters: parameters)
        .responseToJSON({ (_, res, json, error) in
          if error != nil || !self.checkErrorResponse(res) {
            observer.sendFailed(NetworkError(error: error ?? self.fakeError))
          } else {
            observer.sendNext(json)
            observer.sendCompleted()
          }
        })
    }
  }
  
  public func PUT(url url: String, parameters: [String : AnyObject]?) -> SignalProducer<JSON, NetworkError> {
//    if let account = AccountModel.singleton.account {
//      Alamofire.Manager.sharedInstance.session.configuration.HTTPAdditionalHeaders = ["Api-Key": "\(account.token)"]
//    }
    return SignalProducer { observer, disposable in
      Alamofire.request(.PUT, url, parameters: parameters)
        .responseToJSON({ (_, res, json, error) in
          if !self.checkErrorResponse(res){
            observer.sendFailed(NetworkError(error: error ?? self.fakeError))
          } else {
            observer.sendNext(json)
            observer.sendCompleted()
          }
        })
    }
  }
  
  public func DELETE(url url: String, parameters: [String : AnyObject]?) -> SignalProducer<JSON, NSError>{
//    if let account = AccountModel.singleton.account {
//      Alamofire.Manager.sharedInstance.session.configuration.HTTPAdditionalHeaders = ["Api-Key": "\(account.token)"]
//    }
    return SignalProducer { observer, disposable in
      Alamofire.request(.DELETE, url, parameters: parameters)
        .responseToJSON({ (req, res, json, error) in
          if error != nil || !self.checkErrorResponse(res) {
            let err = NSError(domain: req.URL!.absoluteString, code: res!.statusCode, userInfo: nil)
            observer.sendFailed(err)
          } else {
            observer.sendNext(json)
            observer.sendCompleted()
          }
        })
    }
  }
  
  public func Multipart(urlString: String, parameters: Dictionary<String, AnyObject>?, imageData: NSData) -> SignalProducer<JSON, NetworkError> {
//    if let account = AccountModel.singleton.account {
//      Alamofire.Manager.sharedInstance.session.configuration.HTTPAdditionalHeaders = ["Api-Key": "\(account.token)"]
//    }
    let urlRequest = urlRequestWithComponents(urlString, parameters: parameters!, imageData: imageData)
    return SignalProducer { observer, disposable in
      Alamofire.upload(urlRequest.0, data: urlRequest.1)
        .responseToJSON({ (_, res, json, error) in
          if error != nil || !self.checkErrorResponse(res) {
            observer.sendFailed(NetworkError(error: error ?? self.fakeError))
          } else {
            observer.sendNext(json)
            observer.sendCompleted()
          }
        })
    }
  }
		
  func urlRequestWithComponents(urlString:String, parameters:Dictionary<String, AnyObject>, imageData:NSData) -> (URLRequestConvertible, NSData) {
    
    // create url request to observer.send
    let mutableURLRequest = NSMutableURLRequest(URL: NSURL(string: urlString)!)
    mutableURLRequest.HTTPMethod = Alamofire.Method.POST.rawValue
    let boundaryConstant = "myRandomBoundary12345";
    let contentType = "multipart/form-data;boundary="+boundaryConstant
    mutableURLRequest.setValue(contentType, forHTTPHeaderField: "Content-Type")
    
    // create upload data to observer.send
    let uploadData = NSMutableData()
    
    // add image
    uploadData.appendData("\r\n--\(boundaryConstant)\r\n".dataUsingEncoding(NSUTF8StringEncoding)!)
    uploadData.appendData("Content-Disposition: form-data; name=\"background_image\"; filename=\"file.png\"\r\n".dataUsingEncoding(NSUTF8StringEncoding)!)
    uploadData.appendData("Content-Type: image/png\r\n\r\n".dataUsingEncoding(NSUTF8StringEncoding)!)
    uploadData.appendData(imageData)
    
    // add parameters
    for (key, value) in parameters {
      uploadData.appendData("\r\n--\(boundaryConstant)\r\n".dataUsingEncoding(NSUTF8StringEncoding)!)
      uploadData.appendData("Content-Disposition: form-data; name=\"\(key)\"\r\n\r\n\(value)".dataUsingEncoding(NSUTF8StringEncoding)!)
    }
    uploadData.appendData("\r\n--\(boundaryConstant)--\r\n".dataUsingEncoding(NSUTF8StringEncoding)!)
    
    // return URLRequestConvertible and NSData
    return (Alamofire.ParameterEncoding.URL.encode(mutableURLRequest, parameters: nil).0, uploadData)
  }
}
